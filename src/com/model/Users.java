package com.model;//
/*
   Created By: MNFiT3
   Created On: 2018/11/23 10:43 PM
*/

public class Users {
    int id;
    int user_type;
    String name;
    String email;
    String password;
    String salt;

    public Users() {}

    public Users(int id, int user_type, String name, String email, String password, String salt) {
        this.id = id;
        this.user_type = user_type;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salt = salt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}
