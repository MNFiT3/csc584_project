package com.model;//
/*
   Created By: MNFiT3
   Created On: 2018/12/03 5:00 PM
*/

public class Lesson {
    String word;
    String image;
    String sound;
    String video;

    public Lesson() {
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
