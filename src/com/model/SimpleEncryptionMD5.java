package com.model;//
/*
   Created By: MNFiT3
   Created On: 2018/11/22 9:41 PM
   Reference:
   1. https://www.geeksforgeeks.org/md5-hash-in-java/
   2. https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
*/

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class SimpleEncryptionMD5 {
    private static final int RDM_LENGTH = 8;
    private static final String numAlpha = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom sRdm = new SecureRandom();
    private String salt;

    public SimpleEncryptionMD5(){}

    public String generate(String input, String salt){
        byte[] bytesOfMessage;
        if (salt == null){
            input += addSalt();
        } else {
            input += salt;
        }
        try {
            bytesOfMessage = input.getBytes("UTF-8");

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] theDigest = md.digest(bytesOfMessage);

            BigInteger no = new BigInteger(1, theDigest);

            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            return hashtext;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "-1";
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "-1";
        }
    }

    private String addSalt() {
        StringBuilder sb = new StringBuilder(RDM_LENGTH);
        for (int i = 0; i < RDM_LENGTH; i++){
            sb.append(numAlpha.charAt(sRdm.nextInt(numAlpha.length())));
        }
        salt = sb.toString();
        return salt;
    }

    public String getSalt(){
        return salt;
    }

    public static void main(String[] args) {
        SimpleEncryptionMD5 se = new SimpleEncryptionMD5();
        System.out.println(se.generate("abc", null));
        System.out.println(se.getSalt());
    }
}
