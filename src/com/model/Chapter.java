package com.model;//
/*
   Created By: MNFiT3
   Created On: 2018/11/23 10:45 PM
*/

public class Chapter {
    int chapterID;
    String chapterID2;
    String desc;
    String title;
    String createdBy;
    int createdBy_ID;
    String createdOn;


    public Chapter() {
    }

    public String getChapterID2() {
        return chapterID2;
    }

    public void setChapterID2(String chapterID2) {
        this.chapterID2 = chapterID2;
    }

    public int getCreatedBy_ID() {
        return createdBy_ID;
    }

    public void setCreatedBy_ID(int createdBy_ID) {
        this.createdBy_ID = createdBy_ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getChapterID() {
        return chapterID;
    }

    public void setChapterID(int chapterID) {
        this.chapterID = chapterID;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "Chapter{" +
                ", chapterID=" + chapterID +
                ", desc='" + desc + '\'' +
                ", title='" + title + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdOn='" + createdOn + '\'' +
                '}';
    }
}
