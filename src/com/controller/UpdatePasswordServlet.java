package com.controller;//
/*
   Created By: MNFiT3
   Created On: 2018/12/09 2:18 PM
*/

import com.model.SimpleEncryptionMD5;
import com.model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "UpdatePasswordServlet")
public class UpdatePasswordServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        String _oldPassword = request.getParameter("oldPassword");
        String _newPassword = request.getParameter("newPassword");
        String _reNewPassword = request.getParameter("reNewPassword");

        if (_oldPassword != null){
            if (_newPassword.equals(_reNewPassword)){
                Users users = (Users)request.getSession().getAttribute("USERS_OBJ");
                String salt = users.getSalt();
                _oldPassword = new SimpleEncryptionMD5().generate(_oldPassword, salt);

                if (_oldPassword.equals(users.getPassword())){
                    SimpleEncryptionMD5 enc = new SimpleEncryptionMD5();
                    users.setPassword(enc.generate(_newPassword, enc.getSalt()));
                    users.setSalt(enc.getSalt());

                    try {
                        Connection conn = DriverManager.getConnection(getServletContext().getInitParameter("DB_URL"), getServletContext().getInitParameter("DB_USERNAME"), getServletContext().getInitParameter("DB_PASSWORD"));
                        String sql = "UPDATE users SET USERS_PASSWORD = ?, USERS_SALT = ? WHERE users.USERS_ID = ?";
                        PreparedStatement pstmt = conn.prepareStatement(sql);
                        pstmt.setString(1, users.getPassword());
                        pstmt.setString(2, users.getSalt());
                        pstmt.setInt(3, users.getId());
                        pstmt.execute();

                        request.getSession().setAttribute("USERS_OBJ", users);

                        //Successfully changed password
                        response.sendRedirect("../view/updatePassword.jsp?msg=4");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                }else{
                    //Old password not match
                    response.sendRedirect("../view/updatePassword.jsp?msg=5");
                }
            }else{
                //New password not match
                response.sendRedirect("../view/updatePassword.jsp?msg=6");
            }
        }else{
            //Failed to process
            response.sendRedirect("../view/updatePassword.jsp?msg=7");
        }

        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
