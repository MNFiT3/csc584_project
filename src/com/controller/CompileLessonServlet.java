package com.controller;//

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.*;
import java.util.*;

import com.model.Chapter;
import com.model.Lesson;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@WebServlet(name = "CompileLessonServlet")
public class CompileLessonServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private ServletFileUpload uploader = null;
    @Override
    public void init() throws ServletException{
        DiskFileItemFactory fileFactory = new DiskFileItemFactory();
        File filesDir = (File) getServletContext().getAttribute("FILES_DIR_FILE");
        fileFactory.setRepository(filesDir);
        this.uploader = new ServletFileUpload(fileFactory);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String PATH = request.getServletContext().getRealPath("");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if(!ServletFileUpload.isMultipartContent(request)){
            throw new ServletException("Content type is not multipart/form-data");
        }

        try {
            List<FileItem> fileItemsList = uploader.parseRequest(request);
            Iterator<FileItem> fileItemsIterator = fileItemsList.iterator();
            Lesson les = new Lesson();
            while(fileItemsIterator.hasNext()){

                FileItem fileItem = fileItemsIterator.next();

                //out.println(fileItem.toString() + "<hr />");

                if (fileItem.getFieldName().equals("lesson_word")){
                    les.setWord(fileItem.getString());
                }

                if (fileItem.getFieldName().equals("lesson_image") || fileItem.getFieldName().equals("lesson_sound")){
                    String fileName = fileItem.getName();
                    String extension = "";

                    UUID uuid = UUID.randomUUID();
                    int i = fileName.lastIndexOf('.');
                    if (i > 0) {
                        extension = fileName.substring(i+1);
                    }

                    fileName = uuid + "." + extension;

                    switch (extension){
                        case "jpg":
                        case "img":
                        case "gif":
                        case "png":  les.setImage(fileName);break;
                        case "m4a":
                        case "mp3": les.setSound(fileName); break;
                        default:
                    }

                    File file = new File(PATH + File.separator + getServletContext().getInitParameter("dir_upload") + File.separator + fileName);
                    fileItem.write(file);
                }
            }

            HttpSession session = request.getSession();
            Chapter chap = (Chapter)session.getAttribute("chapterBean");

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(getServletContext().getInitParameter("DB_URL"), getServletContext().getInitParameter("DB_USERNAME"), getServletContext().getInitParameter("DB_PASSWORD"));
            String sql = "INSERT INTO `lessons` (`LESSONS_ID`, `LESSONS_QUESTION`, `LESSONS_ANSWER`, `LESSONS_SOUND_URL`, `LESSONS_IMAGE_URL`, `LESSONS_VIDEO_URL`, `LESSONS_BOOK_ID`) VALUES (NULL, ?, ?, ?, ?, ?, ?);";
            PreparedStatement pre = conn.prepareStatement(sql);

            pre.setString(1, les.getWord());      //question
            pre.setString(2, "");              //answer
            pre.setString(3, les.getSound());     //sound
            pre.setString(4, les.getImage());     //image
            pre.setString(5, les.getVideo());     //video
            pre.setInt(6, chap.getChapterID());   //book id

            pre.executeUpdate();
            response.sendRedirect(request.getContextPath() + "/view/create_lesson_1?status=1&word="+les.getWord());

            conn.close();
        } catch (FileUploadException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
