package com.controller;//
/*
   Created By: MNFiT3
   Created On: 2018/11/23 10:48 PM
*/

import com.model.SimpleEncryptionMD5;
import com.model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "RegisterServlet")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Users newUser = new Users();
        newUser.setName(request.getParameter("r_username"));
        newUser.setEmail(request.getParameter("r_email"));
        newUser.setPassword(request.getParameter("r_password"));

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(getServletContext().getInitParameter("DB_URL"), getServletContext().getInitParameter("DB_USERNAME"), getServletContext().getInitParameter("DB_PASSWORD"));
            Statement stmt = conn.createStatement();
            String sql = "SELECT COUNT(USERS_ID) FROM users WHERE USERS_EMAIL = '" + newUser.getEmail() + "'";
            //out.println(sql);
            ResultSet rs = stmt.executeQuery(sql);

            boolean isDuplicate = false;
            while(rs.next()){
                if (rs.getInt(1) != 0){
                    isDuplicate = true;
                }
            }

            if (!isDuplicate){
                SimpleEncryptionMD5 se = new SimpleEncryptionMD5();
                newUser.setPassword(se.generate(newUser.getPassword(), null));
                newUser.setSalt(se.getSalt());

                String query = "INSERT INTO `users` " +
                        "(`USERS_ID`, `USERS_TYPE_ID`, `USERS_NAME`, `USERS_EMAIL`, `USERS_PASSWORD`, `USERS_SALT`) " +
                        "VALUES (NULL, '2', ?, ?, ?, ?)";

                //out.println(query);

                PreparedStatement preparedStatement = conn.prepareStatement(query);
                preparedStatement.setString(1, newUser.getName());
                preparedStatement.setString(2, newUser.getEmail());
                preparedStatement.setString(3, newUser.getPassword());
                preparedStatement.setString(4, newUser.getSalt());

                preparedStatement.execute();

                response.sendRedirect("../view/register.jsp?msg=8");
            }else{
                response.sendRedirect("../view/register.jsp?msg=9");
            }


            conn.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
