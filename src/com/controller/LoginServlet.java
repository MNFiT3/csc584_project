package com.controller;//
/*
   Created By: MNFiT3
   Created On: 2018/11/22 8:43 PM
*/
import com.model.SimpleEncryptionMD5;
import com.model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "Login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String userEmail = request.getParameter("userEmail");
        String userPassword = request.getParameter("password");
        String _userSalt = null;
        String _userPassword = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(getServletContext().getInitParameter("DB_URL"), getServletContext().getInitParameter("DB_USERNAME"), getServletContext().getInitParameter("DB_PASSWORD"));
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users WHERE USERS_EMAIL = '" + userEmail + "'");

            Users user = null;

            while(rs.next()){
                _userPassword = rs.getString("USERS_PASSWORD");
                _userSalt = rs.getString("USERS_SALT");

                user = new Users(
                        rs.getInt("USERS_ID"),
                        rs.getInt("USERS_TYPE_ID"),
                        rs.getString("USERS_NAME"),
                        rs.getString("USERS_EMAIL"),
                        rs.getString("USERS_PASSWORD"),
                        rs.getString("USERS_SALT")
                );
            }

            SimpleEncryptionMD5 se = new SimpleEncryptionMD5();
            userPassword = se.generate(userPassword, _userSalt);

            if(user != null){
                if (_userPassword.equals(userPassword)){
                    out.println("Successfully Login");

                    session.setAttribute("USERS_OBJ", user);

                    if (user.getUser_type() == 1){
                        response.sendRedirect(request.getContextPath() + "/view/admin");
                    }else if (user.getUser_type() == 2){
                        response.sendRedirect(request.getContextPath() + "/view/teacher");
                    }else{
                        response.sendRedirect("../view/login?msg=1");
                    }
                }else{
                    response.sendRedirect("../view/login?msg=2");
                }
            }else{
                response.sendRedirect("../view/login?msg=3");
            }

            conn.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        out.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
