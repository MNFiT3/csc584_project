<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${sessionScope.get('USERS_OBJ') == null}">
    <jsp:forward page="error_login.html"></jsp:forward>
</c:if>