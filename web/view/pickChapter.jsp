<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>
    <body class="background-login" background="../background/homepagebg.jpg">
        <jsp:include page="navbar_student.html"></jsp:include>
        <br /><br />
        <div style="background-color: white">
            <h1 class="luminari" align="center">Choose a lesson!</h1>
        </div>
        <div class="container" align="center">
            <!-- Start -->
            <br /><br />
            <div class="row">
                <sql:query var="result" dataSource="${myDataSource}">
                    SELECT * FROM lesson_category
                </sql:query>
                <c:forEach var="row" items="${result.rowsByIndex}">
                    <div class="col card btn-primary" style="color: black" onclick="chooseChapter(${row[0]})">
                        ${row[1]}
                    </div>
                </c:forEach>
            </div>
            <!-- End -->
        </div>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script>
            function chooseChapter(chap){
                window.location.href = "pickLessons_.jsp?chapter=" + chap;
            }
        </script>
    </body>
</html>