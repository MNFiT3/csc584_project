<jsp:include page="session.jsp"></jsp:include>
        <nav class="navbar navbar-dark bg-dark">
           <a class="navbar-brand" href="#">Teacher</a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
           </button>
           <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                 <a class="nav-item nav-link active" href="dashboard_teacher.jsp">Home <span class="sr-only">(current)</span></a>
                 <a class="nav-item nav-link" href="create_lesson_0">Create Lesson</a>
                 <a class="nav-item nav-link" href="search_lesson.jsp">Search/Delete Lesson</a>
                 <a class="nav-item nav-link" href="updatePassword.jsp">Change Password</a>
                 <a class="nav-item nav-link" href="logout">Logout</a>
              </div>
           </div>
        </nav>