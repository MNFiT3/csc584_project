<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>
    <body>
        <jsp:include page="navbar_student.html"></jsp:include>
        <br />
        <div class="container">
            <div class="container">
                <c:choose>
                    <c:when test="${param.chapter != null}">
                        <sql:query var="result" dataSource="${myDataSource}">
                            SELECT * FROM lesson_book WHERE LESSON_BOOK_CATEGORY_ID = ${param.chapter}
                        </sql:query>
                        <c:forEach var="row" items="${result.rowsByIndex}">
                            <br />
                            <div class="card btn-primary" style="color: black" onclick="chooseLesson(${row[0]})">
                                Title: ${row[2]} <br />
                                Description: ${row[3]} <br />
                                Date Created: ${row[5]} <br />
                                Views: ${row[4]} <br />
                                <!-- Created By: ${row[6]} <br / -->
                            </div>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                        <div class="col" align="center">
                            <h2>Sorry...Something went wrong</h2>
                            <a href="pickChapter.jsp" class="btn btn-primary">Click here to go back</a>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script>
            function chooseLesson(chap){
                window.location.href = "lesson.jsp?bookID=" + chap;
            }
        </script>
    </body>
</html>