<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <script>
            var lessonData = [];
            <c:if test="${param.bookID != null}">
                <sql:query var="res" dataSource="${myDataSource}">
                    SELECT COALESCE(LESSON_BOOK_VIEWS, 0) AS Views FROM lesson_book WHERE lesson_book_id = ${param.bookID}
                </sql:query>
                <c:forEach var="row0" items="${res.rowsByIndex}">
                    <sql:update dataSource = "${myDataSource}" var = "count">
                UPDATE lesson_book SET LESSON_BOOK_VIEWS = (1+${row0[0]}) WHERE LESSON_BOOK_ID = ${param.bookID}
                    </sql:update>
                </c:forEach>

                <sql:query var="result" dataSource="${myDataSource}">
                SELECT * FROM lessons WHERE LESSONS_BOOK_ID = ${param.bookID}
                </sql:query>
                <c:forEach var="row" items="${result.rowsByIndex}">
                lessonData.push({
                    id: "${row[0]}",
                    word: "${row[1]}",
                    answer: "${row[2]}",
                    sound: "${row[3]}",
                    image: "${row[4]}"
                });
                </c:forEach>
            </c:if>
        </script>
    </head>
    <body>
    <jsp:include page="navbar_student.html"></jsp:include>
        <c:forEach var="row" items="${result.rowsByIndex}">
            <audio id="${row[0]}">
                <source src="../uploads/${row[3]}">
            </audio>
        </c:forEach>
        <div class="page">
            <div id="page0">
                <div class="container page card">
                    <form>
                        <div class="row">
                            <div class="col">
                                <div id="image" align="center">

                                </div>
                            </div>
                            <div class="col">
                                <div class="word-frame-0">
                                    <h3 id="word"></h3>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div align="center">
                        <div class="row">
                            <div class="col"><button type="button" class="btn btn-primary button-next" onclick="prevPage()">Back</button></div>
                            <div class="col"><button type="button" class="btn btn-primary button-next" onclick="playAudio()">Play</button></div>
                            <div class="col">
                                <button type="button" id="btnNext" class="btn btn-primary button-next" onclick="nextPage()">Next</button>
                                <button type="button" id="btnHome" class="btn btn-primary button-next" onclick="gotoHome()" style="display: none">Home</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script>
            var i = 0;
            var lessonId = -1;
            var color;
            /*var randomColor = function() {
                return Math.round(Math.random() * 255);
            };

            color = 'rgba('+randomColor()+', '+randomColor()+', '+randomColor()+', 0.6)';*/



            $("#image").html("<img class='image-frame-0' src='../uploads/" + lessonData[0].image + "' width='300px' height='300px'>");
            $("#word").html(lessonData[0].word);

            function nextPage(){
                if (i < lessonData.length){
                    i++;
                    $("#image").html("<img class='image-frame-0' src='../uploads/" + lessonData[i].image + "' width='300px' height='300px'>");
                    $("#word").html(lessonData[i].word);
                    lessonId = lessonData[i].id;
                }

                if(i >= (lessonData.length-1)){
                    $("#btnHome").css("display", "block");
                    $("#btnNext").css("display", "none");
                }
            }

            function prevPage(){
                if (i == 0){
                    gotoHome();
                    return;
                }

                if (i != -1){
                    i--;
                    $("#image").html("<img class='image-frame-0' src='../uploads/" + lessonData[i].image + "' width='300px' height='300px'>");
                    $("#word").html(lessonData[i].word);
                    lessonId = lessonData[i].id;
                    $("#btnHome").css("display", "none");
                    $("#btnNext").css("display", "block");
                }
            }

            function gotoHome(){
                window.location.href = "../view/pickChapter.jsp";
            }

            function playAudio() {
                document.getElementById(lessonId).play();
            }
            function stopAudio(){
                document.getElementById(lessonId).pause()
            }
        </script>
    </body>
</html>