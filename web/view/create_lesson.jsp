<%@ page import="com.model.Users" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.UUID" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<c:if test="${param.btn != null}">
    <jsp:useBean id="chapterBean" class="com.model.Chapter">
        <jsp:setProperty name="chapterBean" property="chapterID" value="${param.lesson_chapter}"></jsp:setProperty>
        <jsp:setProperty name="chapterBean" property="title" value="${param.lesson_title}"></jsp:setProperty>
        <jsp:setProperty name="chapterBean" property="desc" value="${param.lesson_desc}"></jsp:setProperty>
    </jsp:useBean>
    <%
        Date date = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        UUID uuid = UUID.randomUUID();

        chapterBean.setCreatedOn(ft.format(date));
        chapterBean.setCreatedBy_ID(((Users)session.getAttribute("USERS_OBJ")).getId());
        chapterBean.setCreatedBy(((Users)session.getAttribute("USERS_OBJ")).getName());
        chapterBean.setChapterID2(uuid + "");

    %>
    <sql:update dataSource = "${myDataSource}" var = "count">
        INSERT INTO `lesson_book` (`LESSON_BOOK_ID`, `LESSON_BOOK_ID2`, `LESSON_BOOK_NAME`, `LESSON_BOOK_DESC`, `LESSON_BOOK_DATE`, `LESSON_BOOK_AUTHOR_ID`, `LESSON_BOOK_CATEGORY_ID`)
        VALUES (NULL,
        '<jsp:getProperty name="chapterBean" property="chapterID2"></jsp:getProperty>',
        '<jsp:getProperty name="chapterBean" property="title"></jsp:getProperty>',
        '<jsp:getProperty name="chapterBean" property="desc"></jsp:getProperty>',
        '<jsp:getProperty name="chapterBean" property="createdOn"></jsp:getProperty>',
        '<jsp:getProperty name="chapterBean" property="createdBy_ID"></jsp:getProperty>',
        '<jsp:getProperty name="chapterBean" property="chapterID"></jsp:getProperty>');
    </sql:update>
    <sql:query var="result" dataSource="${myDataSource}">
        SELECT LESSON_BOOK_ID FROM lesson_book WHERE LESSON_BOOK_ID2 = '<jsp:getProperty name="chapterBean" property="chapterID2"></jsp:getProperty>'
    </sql:query>
    <c:forEach var="row" items="${result.rowsByIndex}">
        <jsp:setProperty name="chapterBean" property="chapterID" value="${row[0]}"></jsp:setProperty>
    </c:forEach>
    <%  session.setAttribute("chapterBean", chapterBean); %>
    <script>
        alert("Successfully Created");
        window.location.href = "../view/create_lesson_1";
        <%--jsp:forward page="/view/create_lesson_1"></jsp:forward--%>
    </script>

</c:if>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>
    <body>
    <jsp:include page="navbar_teacher.jsp"></jsp:include>
        <div class="page">
            <div class="container page card">
                <h1>Create a lesson</h1>
                <hr />
                <form method="post">
                    <div class="form-group">
                        <label>Chapter :</label>
                        <select class="form-control" name="lesson_chapter">
                            <sql:query var="result" dataSource="${myDataSource}">
                                SELECT * FROM lesson_category
                            </sql:query>
                                <c:forEach var="row" items="${result.rowsByIndex}">
                                    <option value="${row[0]}">${row[1]}</option>
                                </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Lesson Title</label>
                        <input type="text" name="lesson_title" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="lesson_desc" class="form-control">
                    </div>
                    <br />
                    <button type="submit" name="btn" value="" class="btn btn-primary float-R">Next</button>
                </form>
            </div>
        </div>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>