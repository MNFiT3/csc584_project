<jsp:include page="session.jsp"></jsp:include>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
    </head>
    <body>
        <jsp:include page="navbar_admin.jsp"></jsp:include>
        <br />
        <div class="container card">
            <div id="users">

                <!-- class="search" automagically makes an input a search field. -->
                <input class="search form-control" placeholder="Search" /><br />
                <!-- class="sort" automagically makes an element a sort buttons. The date-sort value decides what to sort by. -->
                <!-- button class="sort btn btn-info" data-sort="name">
                    Sort
                </button -->
                <hr />
                <!-- Child elements of container with class="list" becomes list items -->
                <ul class="list">
                    <sql:query var="result" dataSource="${myDataSource}">
                        SELECT USERS_NAME, USERS_EMAIL, USERS_ID  FROM USERS;
                    </sql:query>
                    <c:forEach var="row" items="${result.rowsByIndex}">
                        <li>
                            <h3 class="userName">${row[0]}</h3>
                            <p class="userEmail">User Email: ${row[1]}</p>
                            <p class="userID">User ID: ${row[2]}</p>
                        </li>
                    </c:forEach>
                </ul>

            </div>
        </div>

        <script src="../assets/js/list.js"></script>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script>
            var options = {
                valueNames: [ 'userID', 'userEmail', 'userName' ]
            };

            var userList = new List('users', options);
        </script>
    </body>
</html>