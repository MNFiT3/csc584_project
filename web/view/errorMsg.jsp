<%--
  Created by IntelliJ IDEA.
  User: MNFiT3
  Date: 12/9/2018
  Time: 9:03 PM
  To change this template use File | Settings | File Templates.
--%>
<script>
    if(${param.msg != null}){
        var msg = ${param.msg};
        switch (msg) {
            case 1: alert("Unknown user type!"); break;
            case 2: alert("Invalid Credentials!"); break;
            case 3: alert("User not exist"); break;
            case 4: alert("Successfully changed password"); break;
            case 5: alert("Old password not match"); break;
            case 6: alert("New password not match"); break;
            case 7: alert("Failed to process"); break;
            case 8: alert("Register successfully"); break;
            case 9: alert("Email has been registered"); break;
        }
    }
</script>