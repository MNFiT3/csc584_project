<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <jsp:include page="errorMsg.jsp"></jsp:include>
    </head>
    <body class="background-login">
        <br /><br /><br />
        <h1 align="center" class="luminari">Grammar Tunes Teacher's Quarter</h1>
        <div id="div-login">
            <div class="card login">
                <form action="login_in" method="post">
                    <br />
                    <div class="row">
                        <div class="col"></div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="userEmail" autocomplete="off" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" id="password" name="password" class="form-control" required>
                            </div>
                            <div align="right">
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                        </div>
                        <div class="col"></div>
                    </div>
                    <br />
                </form>
            </div>
            <!--<div class="login">
                <a href="#register"><u>New User? Click To Register... &rarr;</u></a>
            </div>-->
        </div>
        <div align="center">
            <br />
            <a href="../" class="btn btn-info" role="button">Home</a>
        </div>
        <!--<div id="div-register" style="display: none">
            <div class="card login">
                <h4>Register Teacher</h4>
                <hr />
                <form id="register-form" method="post" action="#">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" id="r_email" name="r_email" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" id="r_username" name="r_username" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" id="r_password" name="r_password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Re-enter Password</label>
                        <input type="password" id="r_password2" name="r_password2" class="form-control" required>
                    </div>
                    <button type="button" id="btn-submit" class="btn btn-primary">Register</button>
                </form>
            </div>
            <div class="login">
                <a href="#login"><u>&larr; back to login...</u></a>
            </div>
        </div>-->
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script>
            $(document).ready(function(){

                $('a[href="#register"]').click(function(){
                    animateLogReg();
                });
                $('a[href="#login"]').click(function(){
                    animateLogReg();
                });

                $("#btn-submit").click(function () {
                    if ($("#r_password").val() == $("#r_password2").val()){

                    }else{
                        alert("Password is not the same!")
                    }
                });
                
                function animateLogReg(){
                    $("#div-register").animate({
                        height: 'toggle'
                    });
                    $("#div-login").animate({
                        height: 'toggle'
                    });
                }
            })
        </script>
    </body>
</html>