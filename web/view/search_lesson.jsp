<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.SQLException" %>
<jsp:include page="session.jsp"></jsp:include>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<c:set var="userObj" value="${sessionScope.get('USERS_OBJ')}"></c:set>
<c:if test="${param.bkID != null}">
    <%
        try {
            int bkID = Integer.parseInt(request.getParameter("bkID"));

            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/grammartunes", "root", "");
            String sql = "DELETE FROM `lessons` WHERE `LESSONS_BOOK_ID` = ?";
            String sql2 = "DELETE FROM `lesson_book` WHERE `lesson_book`.`LESSON_BOOK_ID` = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            PreparedStatement pstmt2 = conn.prepareStatement(sql2);
            pstmt.setInt(1, bkID);
            pstmt2.setInt(1, bkID);

            pstmt.execute();
            pstmt2.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    %>
    <script>
        alert("Successfully Deleted");
        window.location.replace("../view/dashboard_teacher.jsp");
    </script>
</c:if>

<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <style>
            button{
                float: right;
            }
        </style>
    </head>
    <body>
        <jsp:include page="navbar_teacher.jsp"></jsp:include>
        <br />
        <div class="container card">
            <div id="chapter">
                <input class="search form-control" placeholder="Search" /><br />
                <hr />
                <ul class="list">
                    <sql:query var="result" dataSource="${myDataSource}">
                        SELECT
                            `LESSON_BOOK_NAME`,
                            `LESSON_BOOK_DESC`,
                            `LESSON_BOOK_VIEWS`,
                            `LESSON_BOOK_DATE`,
                            `USERS_NAME`,
                            `LESSON_BOOK_ID`
                        FROM
                            lesson_book lb,
                            users u
                        WHERE
                            lb.LESSON_BOOK_AUTHOR_ID = u.USERS_ID
                        AND
                            u.USERS_ID =  ${userObj.getId()}
                    </sql:query>
                    <c:forEach var="row" items="${result.rowsByIndex}">
                        <li>
                            <button type="button" class="btn btn-danger" onclick="del(${row[5]})">Delete</button>
                            <h3 class="title">${row[0]}</h3>
                            <div class="desc">Description: ${row[1]}</div>
                            <div class="">Views: ${row[2]}</div>
                            <div class="date">Created On: ${row[3]}</div>
                            <div class="">Created By: ${row[4]}</div>
                        </li>
                        <hr />
                    </c:forEach>
                </ul>

            </div>
        </div>

        <script src="../assets/js/list.js"></script>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script>
            var options = {
                valueNames: [ 'title', 'desc', 'date' ]
            };

            new List('chapter', options);

            function del(bkID){
                if (confirm("Do you want to delete this lesson?")) {
                    window.location.replace("../view/search_lesson.jsp?bkID=" + bkID)
                }
            }
        </script>
    </body>
</html>