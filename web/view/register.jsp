<htm>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <jsp:include page="errorMsg.jsp"></jsp:include>
    </head>
    <body class="background-login">
    <jsp:include page="navbar_admin.jsp"></jsp:include>
        <br /><br /><br />
        <h1 align="center" class="luminari">Grammar Tunes Teacher's Quarter</h1>
        <div id="div-register">
            <div class="card login">
                <h4>Register Teacher</h4>
                <hr />
                <form method="post" action="register">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" id="r_email" name="r_email" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" id="r_username" name="r_username" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" id="r_password" name="r_password" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Re-enter Password</label>
                        <input type="password" id="r_password2" name="r_password2" class="form-control" required>
                    </div>
                    <button type="submit" id="btn-submit" class="btn btn-primary">Register</button>
                </form>
            </div>
        </div>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</htm>