<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <jsp:include page="errorMsg.jsp"></jsp:include>
    </head>
    <body>

        <br />
        <div class="container card">
            <form method="post" action="update_password">
                <div class="form-group">
                    <label>Old Password</label>
                    <input type="password" name="oldPassword" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" name="newPassword" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Re-enter new password</label>
                    <input type="password" name="reNewPassword" class="form-control" required>
                </div>
                <button type="submit" name="btn" value="" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>