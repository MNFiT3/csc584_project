<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sql:setDataSource var="myDataSource" driver="com.mysql.jdbc.Driver" url="jdbc:mysql://localhost:3306/grammartunes" user="root" password=""/>
<html>
    <head>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/style.css">
        <script>
            var sumAllChapter = 0;
            var totalViewChapter = [];
            var chapter = [];
            <sql:query var="sumViews0" scope="page" dataSource="${myDataSource}">
                SELECT SUM(COALESCE(LESSON_BOOK_VIEWS, 0)) As sumOfChap FROM lesson_book ORDER BY LESSON_BOOK_VIEWS ASC
            </sql:query>
            <c:forEach var="row" items="${sumViews0.rowsByIndex}">
                sumAllChapter = ${row[0]};
            </c:forEach>
            <sql:query var="totalEachChap" scope="page" dataSource="${myDataSource}">
            SELECT COALESCE(LESSON_BOOK_VIEWS, 0) As totalOfEachChap FROM lesson_book GROUP BY LESSON_BOOK_CATEGORY_ID ORDER BY LESSON_BOOK_CATEGORY_ID ASC
            </sql:query>
            <c:forEach var="row" items="${totalEachChap.rowsByIndex}">
                totalViewChapter.push(${row[0]});
            </c:forEach>
            <sql:query var="totalEachChap" scope="page" dataSource="${myDataSource}">
                SELECT
                    lc.LESSON_CATEGORY_NAME
                FROM
                    lesson_book lb,
                    lesson_category lc
                WHERE
                    lc.LESSON_CATEGORY_ID = lb.LESSON_BOOK_CATEGORY_ID
                GROUP BY
                    lc.LESSON_CATEGORY_ID
                ORDER BY
                    lc.LESSON_CATEGORY_ID ASC
            </sql:query>
            <c:forEach var="row" items="${totalEachChap.rowsByIndex}">
                chapter.push("${row[0]}");
            </c:forEach>
            console.log(totalViewChapter)
            console.log(chapter)
        </script>
    </head>
    <body>
    <jsp:include page="navbar_admin.jsp"></jsp:include>
        <!-- include navbar -->
        <br /><br />
        <div class="container">
            <div id="btnPrint">
                <button  type="button" onclick="printPage()">Print</button>
            </div>
            <div align="center">
                <h1>Total Number of chapter viewed</h1>
            </div>
            <div class="row">
                <div class="col"><canvas id="myChart"></canvas></div>
            </div>
            <hr />
            <div class="row">
                <div class="col"><canvas id="chart-area"></canvas></div>
            </div>
        </div>

        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/Chart.bundle.min.js"></script>
        <script>

        function printPage() {
            window.open("../view/report_total_chapter_print.jsp");
        }

        var randomColor = function() {
            return Math.round(Math.random() * 255);
        };

        var colors = [];
        for(i = 0; i < chapter.length; i++){
            colors.push('rgba('+randomColor()+', '+randomColor()+', '+randomColor()+', 0.6)');
        }

        var e = document.getElementById("myChart");
        //e.style.width = window.innerWidth;
        //e.style.height = window.innerHeight;
        var ctx = e.getContext('2d');

        var e1 = document.getElementById("chart-area");
        //e1.style.width = window.innerWidth;
        //e1.style.height = window.innerHeight;
        var ctx2 = e1.getContext('2d');
            
        var config = {
            type: 'bar',
            data: {
                labels: chapter,
                datasets: [{
                    label: '# of Lessons Views',
                    data: totalViewChapter,
                    backgroundColor: colors,
                    borderColor: colors,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        }

        var config2 = {
            type: 'pie',
            data: {
                datasets: [{
                    data: totalViewChapter,
                    backgroundColor: colors,
                    label: 'Number Of Views By Chapter'
                }],
                labels: chapter
            },
            options: {
                responsive: true
            }
        };
            
        new Chart(ctx, config);
        new Chart(ctx2, config2);
        </script>
    </body>
</html>