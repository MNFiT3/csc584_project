<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
</head>
<body>
<jsp:include page="navbar_teacher.jsp"></jsp:include>
<div>
    <div class="container">
        <br /><br /><br />
        <div class="row">
            <div class="col-2"></div>
            <div class="col card" style="display: block">
                <form action="compile_lesson" method="post" enctype="multipart/form-data">
                    <div class="from-group">
                        <label>Word</label><br />
                        <input name="lesson_word" type="text" class="form-control" required>
                    </div>
                    <hr />
                    Choose(s) any...
                    <hr />
                    <div class="form-group">
                        <labe>Image</labe><br />
                        <input type="file" name="lesson_image">
                    </div>
                    <div class="form-group">
                        <labe>Sound</labe><br />
                        <input type="file" name="lesson_sound">
                    </div>
                    <!--div class="form-group">
                        <labe>Video</labe><br />
                        <input type="file" name="fileVideo">
                    </div-->
                    <br>
                    <button name="status" value="0" type="submit" value="" class="btn btn-primary" style="float: right">Save</button>
                    <button type="button" class="btn btn-primary" onclick="done()">Done</button>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
</div>
<script src="../assets/js/jquery-3.3.1.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script>
    if (${param.status != null && param.word != null}) {
        alert("Word: ${param.word} added successfully");
    }

    function done(){
        if (confirm("Stop adding lessons?")) {
            window.location.href = "../view/create_lesson_0";
        }
    }
</script>
</body>
</html>